import Home from "./pages/Home"
import Footer from "./components/Footer"
import AppNavBar from "./components/AppNavBar"
import React from "react";
import { Container } from "react-bootstrap";



import {UserProvider} from "./UserContext"
import data from "./data/apiContent"

import "bootswatch/dist/cosmo/bootstrap.min.css"
import './App.css';

function App() {
  return (
    <UserProvider value={{data}}>
      <AppNavBar/>
      <Container>

        <Home />
      </Container>
      <Footer/>
    </UserProvider>
  );
}

export default App;
