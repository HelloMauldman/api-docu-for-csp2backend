import { Row, Col, Button } from 'react-bootstrap';


const Jumbotron = ()=>{
	return(
		<Row className="py-5 m-5 bLight bg-gradient rounded-3" id="home">
			<Col>
				<div className="text-center display-5 m-5 px-5">
					<h1 className="fs-1 fw-bold">Shinji's E-commerce website API 1.0</h1>
					<p id="motto" className="md-8 fs-4">This a simple API used and built to connect the e-commerce website Front-end to the Back-End.</p>
					<Button className="btn-primary rounded-3" href="#documentation">
						Check out the API Endpoints!</Button> <br/>
					<Button className="btn-primary rounded-3" href="https://csp3-shinji-ecommerce.vercel.app/" target="_poop" rel="noreferrer noopener">
						Check out the e-commerce website!</Button>
				</div>
			</Col>
		</Row>
	);
}

export default Jumbotron;