import React, {useContext, useState} from "react";
import {Table, Button} from "react-bootstrap";
import UserContext from "../UserContext"


function ApiTable(){
	const {data} = useContext(UserContext)
	const {products, users} = data;
	const [toggle, setToggle] = useState(false)
	const [contents, setContents] = useState(products)

	const toggler = ()=>{
		if(toggle === true){
			setToggle(false)
			setContents(products)
		}
		else{
			setToggle(true)
			setContents(users)
		}
	}


	const contentsArr = contents.map((content)=>{
		return(
			<tr key={content.id}>
				<td>{content.description}</td>
				<td>{content.httpVerb}</td>
				<td>{content.admin}</td>
				<td>{content.endpoint}</td>
			</tr>
		)
	})

	return(
		<React.Fragment>
			<div className="justify-content-center d-flex mb-3 ">
			{ toggle === false ?
				<Button className="rounded-3" onClick={()=>toggler()}>For User's API Endpoint</Button>
				:
				<Button className="rounded-3" onClick={()=>toggler()}>For Products's API Endpoint</Button>
			}
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-secondary text-light" id="documentation">
					<tr>
						<th>Description</th>
						<th>HTTP Verb</th>
						<th>Admin</th>
						<th>Endpoint</th>
					</tr>
				</thead>
				<tbody>
					{contentsArr}
				</tbody>
			</Table>
		</React.Fragment>
	)
}

export default ApiTable;