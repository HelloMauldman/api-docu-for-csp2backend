import React from "react";
import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap" 

function AppNavBar(){
	return(
		<Navbar bg="dark" variant="dark" expand="lg" className="fixed-top">
		  <Container>
		    <Navbar.Brand href="#home">E-commerce API Docs</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link href="https://mauldman1.github.io/Capstone-1/" target="_JES" rel="noreferrer">Home</Nav.Link>
		        <NavDropdown title="GitLab" id="basic-nav-dropdown">
		          <NavDropdown.Item href="https://gitlab.com/HelloMauldman/api-docu-for-csp2backend" target="_KOP" rel="noreferrer">Repository for API Doc website</NavDropdown.Item>
		          <NavDropdown.Divider />
		          <NavDropdown.Item href="https://gitlab.com/HelloMauldman/csp3" target="_HooMan" rel="noreferrer">Repository for e-commerce website</NavDropdown.Item>
		        </NavDropdown>
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}

export default AppNavBar;