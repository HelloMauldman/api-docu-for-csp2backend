import { Container } from "react-bootstrap";

function Footer(){
	return(
		<Container>
			<div className="py-3 my-4">
				<ul className="nav justify-content-center border-bottom pb-3 mb-3">
					<li><a href="#home" className="nav-link px-2 text-muted">Home</a></li>
					<li><a href="https://www.facebook.com/shinji.caro" className="nav-link px-2 text-muted" rel="noreferrer" target="_woah">Facebook</a></li>
					<li><a href="https://www.linkedin.com/in/shinji-caro-8147b41a8/" className="nav-link px-2 text-muted" rel="noreferrer" target="_hoof">LinkedIn</a></li>
				</ul>
				<p className="text-center text-muted">&copy; 2021 Shinji C.</p>
			</div>
		</Container>
	)
}

export default Footer;