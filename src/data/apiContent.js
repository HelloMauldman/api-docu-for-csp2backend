const apiContent = {
	products:[
	{
		id:"1",
		description:"Get All Products",
		httpVerb:"GET",
		admin:"NO",
		endpoint:"/products/all"
	},
	{
		id:"2",
		description:"Update Products",
		httpVerb:"PUT",
		admin:"YES",
		endpoint:"/products/:productId"
	},
	{
		id:"3",
		description:"Get All Active Products",
		httpVerb:"GET",
		admin:"NO",
		endpoint:"/products/active"
	},
	{
		id:"4",
		description:"Add Products",
		httpVerb:"POST",
		admin:"YES",
		endpoint:"/products/"
	},
	{
		id:"5",
		description:"Get Single Products",
		httpVerb:"GET",
		admin:"NO",
		endpoint:"/products/:productId"
	},
	{
		id:"6",
		description:"Archive Products",
		httpVerb:"PUT",
		admin:"YES",
		endpoint:"/products/:productId/archive"
	},
	{
		id:"7",
		description:"Activate Products",
		httpVerb:"PUT",
		admin:"YES",
		endpoint:"/products/:productId/ctivate"
	}
	],
	users:[
	{
		id:"1",
		description:"Register User",
		httpVerb:"POST",
		admin:"NO",
		endpoint:"/users/register"
	},
	{
		id:"2",
		description:"Login User",
		httpVerb:"POST",
		admin:"NO",
		endpoint:"/users/login"
	},
	{
		id:"3",
		description:"Set Admin",
		httpVerb:"PUT",
		admin:"YES",
		endpoint:"/users/:userId/setAsAdmin"
	},
	{
		id:"4",
		description:"Checkout User Orders",
		httpVerb:"POST",
		admin:"NO",
		endpoint:"/users/checkout"
	},
	{
		id:"5",
		description:"Get Orders of User",
		httpVerb:"GET",
		admin:"NO",
		endpoint:"/users/myOrders"
	},
	{
		id:"6",
		description:"Get All Orders",
		httpVerb:"GET",
		admin:"YES",
		endpoint:"/users/orders"
	},
	{
		id:"7",
		description:"Get Profile User",
		httpVerb:"GET",
		admin:"NO",
		endpoint:"/users/details"
	}
	]
}

export default apiContent;
