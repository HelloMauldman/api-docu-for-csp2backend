import React, {useState, useRef} from "react";
import { Container, Overlay, Button} from "react-bootstrap";
import ApiTable from "../components/ApiTable"
import Jumbotron from "../components/Jumbotron"

function Home(){
	const [show, setShow] = useState(false);
	const target = useRef(null);
	return(
		<React.Fragment>
			<Container fluid>
				<Jumbotron/>
				<div className="mb-5 pb-2">
					<h3>Using the Rest API</h3>
					<p>The API built follows the general patterns of REST. This is also just a Minimum Viable Product (MVP) and is subject to further improvement.</p>
					<h3>Authorizations, Access and others</h3>
					<p>Since this is a MVP, this is a simple project that utilizes JSON JWT tokens as a means to access the other endpoints through the access tokens generated when logging in. However, there are only some endpoints that can be accessed by a regular user and that for the generated Authorization Tokens when logging in, it includes and checks whether the token contains the information if this is an admin or a regular user.</p>
					<h3>Testing in Postman</h3>
					<p>To test this API, you can use the Postman. You can use the <span className="fw-bold">https://enigmatic-mesa-25753.herokuapp.com</span> as the variable or link in accessing the Back End/API.
					<br/>
					<br/>
						Then simply register a new account through the Register user endpoint to get the personal access token. For accessing as the admin, <Button className="border-0" variant="outline-primary rounded-3" ref={target} onClick={() => setShow(!show)}>
							click here.
							</Button>
					</p>
					
					<Overlay target={target.current} show={show} placement="right">
						{({ placement, arrowProps, show: _show, popper, ...props }) => (
							<div
								{...props}
									style={{
									backgroundColor: 'rgba(255, 100, 100, 0.85)',
									padding: '2px 10px',
									color: 'white',
									borderRadius: 3,
									...props.style,
									}}
								>
								"email":"admin@mail.com",<br/>
								"password":"admin1234"
							</div>
						)}
					</Overlay>

					</div>
					<ApiTable/>
					</Container>
		</React.Fragment>
	)
}

export default Home;